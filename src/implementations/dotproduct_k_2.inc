  subroutine loops_ab(a,b,xloc)
    implicit none
    real(wp), intent(in)           :: a(ba(1):,ba(2):,ba(3):)
    real(wp), intent(in), optional :: b(bb(1):,bb(2):,bb(3):)
    real(wp), intent(inout)        :: xloc

    integer j, k,  s(3), e(3)

    s = [ mg%sx, mg%sy, mg%sz]
    e = [ mg%ex, mg%ey, mg%ez]

    if (present(b)) then
       !$OMP DO SCHEDULE(STATIC) reduction(+:xloc) COLLAPSE(2)
       do k = s(3), e(3)
          do j = s(2), e(2)
             xloc = xloc + scl * sum(a(s(1):e(1), j, k) &
                  * b(s(1):e(1), j, k))
          enddo
       enddo
       !$OMP ENDDO
    else
       !$OMP DO SCHEDULE(STATIC) reduction(+:xloc) COLLAPSE(2)
       do k = s(3), e(3)
          do j = s(2), e(2)
             xloc = xloc + scl * sum(a(s(1):e(1), j, k))
          enddo
       enddo
       !$OMP ENDDO
    endif
  end subroutine loops_ab
