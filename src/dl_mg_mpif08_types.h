#ifdef USE_MPI_F08
#define MP_COMM      type(mpi_comm)
#define MP_REQ       type(mpi_request)
#define MP_STATUS(n) type(mpi_status),dimension(n)
#else
#define MP_COMM      integer
#define MP_REQ       integer
#define MP_STATUS(n) integer,dimension(MPI_STATUS_SIZE,n)
#endif
