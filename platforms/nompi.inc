#============================================================
# Makefile settings to build without MPI
#
# Lucian Anton
# created: 7/02/14
#============================================================

COMP := gnu

ifeq ($(COMP),gnu)
  FC := gfortran
else ifeq ($(COMP),intel)
  FC := ifort
else ifeq ($(COMP),nv)
  FC := nvfortran
else
  $(error "Unknown compiler")
endif

USE_OPENMP = yes

# gfortran flags
FFLAGS_gnu := -pedantic
OMPFLAGS_gnu_yes  := -fopenmp
OMPFLAGS_gnu_no   :=
FFLAGS_gnu_opt    := -O3
FFLAGS_gnu_profile  := -DUSE_TIMER $(FFLAGS_gnu_opt)
# differit gfortran flags for differit compiler version
# is there a environment version variable?
FFLAGS_gnu_debug  := -O0 -g -fbounds-check -Wall -Wuninitialized -Wno-unused -finit-real=nan -ffpe-trap=invalid,zero,overflow -fsignaling-nans -fdump-core -fbacktrace
#FFLAGS_gnu_debug :=  -g -fcheck=all -Wall -Wno-unused -finit-real=snan -ffpe-trap=invalid,zero,overflow -fsignaling-nans -fdump-core -fbacktrace
#FFLAGS_gnu_debug := -g -fcheck=all -Wall -ffpe-trap=invalid,zero,overflow -fsignaling-nans
MODS_DESTINATION_gnu = -J$(OBJDIR)


# intel flags
FFLAGS_intel :=
OMPFLAGS_intel_yes  := -qopenmp
OMPFLAGS_intel_no   :=
FFLAGS_intel_opt    := -g -O3
FFLAGS_intel_profile  := -DUSE_TIMER $(FFLAGS_intel_opt)
FFLAGS_intel_debug := -g -check noarg_temp_created -traceback -DUSE_BACKTARCE
MODS_DESTINATION_intel := -module $(OBJDIR)

# nvfortran flags
FFLAGS_nv :=
FFLAGS_nv_opt := -g -fast
FFLAGS_nv_debug := -g -Mbounds -Mchkptr -Mchkstk -Mcoff -Mdwarf2 -Mdwarf3 -Melf -Mnodwarf -traceback
OMPFLAGS_nv_yes := -mp
OMPFLAGS_nv_no :=
MODS_DESTINATION_nv := -module $(OBJDIR)


# enable lots of prints in V cycle
ifdef RELAX_PRINT
  FRELAX_PRINT := -DDLMG_RELAX_PRINT
endif



FFLAGS := $(FFLAGS_$(COMP)) $(FFLAGS_$(COMP)_$(BUILD)) $(OMPFLAGS_$(COMP)_$(USE_OPENMP)) $(FRELAX_PRINT)

INCPATHS := $(MODS_DESTINATION_$(COMP)) -I$(OBJDIR) -Isrc
