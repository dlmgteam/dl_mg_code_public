#$(error deprecated platform!!!)

#============================================================
# Makefile variable for laptop (gfortran + MPICH2)
#
# Lucian Anton
# created: 20/04/12
#============================================================

FC := mpif90
F77 := mpif77

COMP := gnu

USE_OPENMP = yes

MPIFLAGS := -DMPI

ifdef USE_INCLUDE_MPIF
  MPIFLAGS += -DUSE_INCLUDE_MPIF
endif

ifdef USE_MPI_F08
  MPIFLAGS += -DUSE_MPI_F08
  gnu_lang_check := -pedantic
else
  gnu_lang_check := -fno-strict-aliasing
endif

ifdef TEST_ERR_CODES
  TEST_ERR := -DDL_MG_TEST_ERR_CODES
endif

# GCC flags

FFLAGS_gnu := $(gnu_lang_check)
OMPFLAGS_gnu_yes  := -fopenmp
OMPFLAGS_gnu_no   :=
FFLAGS_gnu_opt    := -g -O3

FFLAGS_gnu_profile  := -DUSE_TIMER $(FFLAGS_gnu_opt)

# enable lots of prints in V cycle
ifdef RELAX_PRINT
  FRELAX_PRINT := -DDLMG_RELAX_PRINT
endif

MODS_DESTINATION_gnu = -J$(OBJDIR)

# -DDUMP_DATA -DUSE_DUMP
FFLAGS_gnu_debug := -g -fcheck=all -Wall -finit-real=snan -ffpe-trap=invalid,zero,overflow -fsignaling-nans -fdump-core -DUSE_BACKTRACE
#FFLAGS_gnu_debug := -g -fcheck=all -Wall -ffpe-trap=invalid,zero,overflow -fsignaling-nans
#FFLAGS_gnu_debug  :=  $(FRELAX_PRINT) -O0 -g -fbounds-check -Wall -Wuninitialized -Wno-unused -finit-real=nan -ffpe-trap=invalid,zero,overflow -fsignaling-nans -fdump-core -fbacktrace

# intel flags

FFLAGS_intel := 
OMPFLAGS_intel_yes  := -qopenmp
OMPFLAGS_intel_no   :=
FFLAGS_intel_opt    := -g -O3 -ftrapuv -init=snan,arrays
FFLAGS_intel_profile  := -DUSE_TIMER $(FFLAGS_intel_opt)
FFLAGS_intel_debug := -g -check noarg_temp_created -traceback -DUSE_BACKTARCE -ftrapuv -init=snan,arrays
MODS_DESTINATION_intel := -module $(OBJDIR)

# nvfortran flags

FFLAGS_nv := 
FFLAGS_nv_opt := -g -fast
FFLAGS_nv_debug := -g -Mbounds -Mchkptr -Mchkstk -Mcoff -Mdwarf2 -Mdwarf3 -Melf -Mnodwarf -traceback
OMPFLAGS_nv_yes := -mp
OMPFLAGS_nv_no := 
MODS_DESTINATION_nv := -module $(OBJDIR)

# aggregated compiler flags

FFLAGS  :=  $(MPIFLAGS) $(FFLAGS_$(COMP)) $(FFLAGS_$(COMP)_$(BUILD)) $(OMPFLAGS_$(COMP)_$(USE_OPENMP)) -DHAVE_CONTIGUOUS $(TEST_ERR)

INCPATHS := $(MODS_DESTINATION_$(COMP)) -I$(OBJDIR) -Isrc
